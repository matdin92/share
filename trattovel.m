function tab = trattovel(dati) %traccia il plot in base alle classi
tab = table;
tab.long = dati.LONG;
tab.lat = dati.LAT;
tab.vel = dati.SPEED;
[id,cen] = kmeans(tab.vel,3);
tab.classe = id;
tab.te = dati.SECONDI;
fin = length(tab.vel);
telim = 660;
for i = 2:(fin)
    deltat = (tab.te(i) - tab.te(i-1));
    if deltat > telim
        tab.tratto(i-1) = 1; % sconosciuto
    end
    if deltat <= telim
        if (tab.classe(i-1) ~= tab.classe(i))
            tab.tratto(i-1) = 2; %incerto
        end
        if (tab.classe(i) == tab.classe(i-1))
            if tab.classe(i-1) == 1
                tab.tratto(i-1) = consp(1,cen);
            end
            if tab.classe(i-1) == 2                
                tab.tratto(i-1) = consp(2,cen); 
            end                        
            if tab.classe(i-1) == 3
                tab.tratto(i-1) = consp(3,cen);                               
            end
        end
    end
end
deltat = (tab.te(fin) - tab.te(fin-1));
    if deltat > telim
        tab.tratto(fin) = 1; % sconosciuto
    end
    if deltat <= telim
        if (tab.classe(fin) ~= tab.classe(fin-1))
            tab.tratto(fin) = 2; %incerto
        end
        if (tab.classe(fin) == tab.classe(fin-1))
            if tab.classe(fin) == 1
                tab.tratto(fin) = consp(1,cen);
            end
            if tab.classe(fin) == 2                
                tab.tratto(fin) = consp(2,cen); 
            end                        
            if tab.classe(fin) == 3
                tab.tratto(fin) = consp(3,cen);                               
            end           
        end
    end
    gra(tab);    
end