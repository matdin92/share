function tab = tratto(dati,c) %traccia il plot in base alle classi
tab = table;
tab.long = dati.LONG;
tab.lat = dati.LAT;
tab.vel = dati.SPEED;
[id,cen] = kmeans(tab.vel,c);
tab.classe = id;
tab.te = dati.SECONDI;
fin = length(tab.vel);
telim = 660;
for u = 2:(fin)
    deltat = (tab.te(u) - tab.te(u-1));
    if deltat > telim
        tab.trattov(u-1) = 1; % sconosciuto
    end
    if deltat <= telim
        if (tab.classe(u-1) ~= tab.classe(u))
            tab.trattov(u-1) = 2; %incerto
        end
        if (tab.classe(u) == tab.classe(u-1))
            if tab.classe(u-1) == 1
                tab.trattov(u-1) = ordin(1,cen);
            end
            if tab.classe(u-1) == 2                
                tab.trattov(u-1) = ordin(2,cen); 
            end                        
            if tab.classe(u-1) == 3
                tab.trattov(u-1) = ordin(3,cen);                               
            end
            if tab.classe(u-1) == 4
                tab.trattov(u-1) = ordin(4,cen);                               
            end
        end
    end
end
deltat = (tab.te(fin) - tab.te(fin-1));
    if deltat > telim
        tab.trattov(fin) = 1; % sconosciuto
    end
    if deltat <= telim
        if (tab.classe(fin) ~= tab.classe(fin-1))
            tab.trattov(fin) = 2; %incerto
        end
        if (tab.classe(fin) == tab.classe(fin-1))
            if tab.classe(fin) == 1
                tab.trattov(fin) = ordin(1,cen);
            end
            if tab.classe(fin) == 2                
                tab.trattov(fin) = ordin(2,cen); 
            end                        
            if tab.classe(fin) == 3
                tab.trattov(fin) = ordin(3,cen);                               
            end
            if tab.classe(fin) == 4
                tab.trattov(fin) = ordin(4,cen);                               
            end 
        end
    end
    
tab.course = dati.COURSE;
fin = length(tab.course);
lim = 40;
for i = 2:(fin)
    if tab.course(i-1) > 270 && tab.course(i) < 90 || tab.course(i-1) < 90 && tab.course(i) > 270
        if tab.course(i-1) < 90
            tab.course(i-1) = tab.course(i-1) + 360;
        end
        if tab.course(i) < 90
            tab.course(i) = tab.course(i) + 360;
        end
    end 
    x(i-1) = abs(tab.course(i) - tab.course(i-1));
    if tab.course(i-1) > 360
       
        tab.course(i-1) = tab.course(i-1) - 360;
    end
    if tab.course(i) > 360
        tab.course(i) = tab.course(i) - 360;        
    end
    if x(i-1) < lim
        y(i-1) = 1; %costante
    else
        y(i-1) = 0; %non costante
    end
end
if tab.course(fin-1) > 270 && tab.course(fin) < 90 || tab.course(fin-1) < 90 && tab.course(fin) > 270
        if tab.course(fin-1) < 90
            tab.course(fin-1) = tab.course(fin-1) + 360;
        end
        if tab.course(fin) < 90
            tab.course(fin) = tab.course(fin) + 360;
        end
end 
if tab.course(fin-1) > 360       
        tab.course(fin-1) = tab.course(fin-1) - 360;
    end
    if tab.course(fin) > 360
        tab.course(fin) = tab.course(fin) - 360;        
    end
x(fin) = abs(tab.course(fin) - tab.course(fin-1));
if x(fin) < lim
        y(fin) = 1; %costante
    else
        y(fin) = 0; %non costante
end
tab.trattoc = y';
istog(tab,c,id);
gra(tab);    
end