clear;
fl = 0;
while fl == 0
filename = input('Inserisci percorso file da analizzare: ','s');
delimiter = ',';
startRow = 2;
formatSpec = '%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string', 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
fclose(fileID);
raw = repmat({''},length(dataArray{1}),length(dataArray)-1);
for col=1:length(dataArray)-1
    raw(1:length(dataArray{col}),col) = mat2cell(dataArray{col}, ones(length(dataArray{col}), 1));
end
numericData = NaN(size(dataArray{1},1),size(dataArray,2));
for col=[1,2,5,6,7,8,9,10,11,14,15,16,17,18,19]
    rawData = dataArray{col};
    for row=1:size(rawData, 1)
        regexstr = '(?<prefix>.*?)(?<numbers>([-]*(\d+[\,]*)+[\.]{0,1}\d*[eEdD]{0,1}[-+]*\d*[i]{0,1})|([-]*(\d+[\,]*)*[\.]{1,1}\d+[eEdD]{0,1}[-+]*\d*[i]{0,1}))(?<suffix>.*)';
        try
            result = regexp(rawData(row), regexstr, 'names');
            numbers = result.numbers;
            invalidThousandsSeparator = false;
            if numbers.contains(',')
                thousandsRegExp = '^\d+?(\,\d{3})*\.{0,1}\d*$';
                if isempty(regexp(numbers, thousandsRegExp, 'once'))
                    numbers = NaN;
                    invalidThousandsSeparator = true;
                end
            end          
            if ~invalidThousandsSeparator
                numbers = textscan(char(strrep(numbers, ',', '')), '%f');
                numericData(row, col) = numbers{1};
                raw{row, col} = numbers{1};
            end
        catch
            raw{row, col} = rawData{row};
        end
    end
end
dateFormats = {'yyyy-MM-dd', 'HH:mm:ss'};
dateFormatIndex = 1;
blankDates = cell(1,size(raw,2));
anyBlankDates = false(size(raw,1),1);
invalidDates = cell(1,size(raw,2));
anyInvalidDates = false(size(raw,1),1);
for col=[3,4]
    try
        dates{col} = datetime(dataArray{col}, 'Format', dateFormats{col==[3,4]}, 'InputFormat', dateFormats{col==[3,4]}); %#ok<SAGROW>
    catch
        try
            dataArray{col} = cellfun(@(x) x(2:end-1), dataArray{col}, 'UniformOutput', false);
            dates{col} = datetime(dataArray{col}, 'Format', dateFormats{col==[3,4]}, 'InputFormat', dateFormats{col==[3,4]}); %#ok<SAGROW>
        catch
            dates{col} = repmat(datetime([NaN NaN NaN]), size(dataArray{col})); %#ok<SAGROW>
        end
    end    
    dateFormatIndex = dateFormatIndex + 1;
    blankDates{col} = dataArray{col} == '';
    anyBlankDates = blankDates{col} | anyBlankDates;
    invalidDates{col} = isnan(dates{col}.Hour) - blankDates{col};
    anyInvalidDates = invalidDates{col} | anyInvalidDates;
end
dates = dates(:,[3,4]);
blankDates = blankDates(:,[3,4]);
invalidDates = invalidDates(:,[3,4]);
rawNumericColumns = raw(:, [1,2,5,6,7,8,9,10,11,14,15,16,17,18,19]);
rawStringColumns = string(raw(:, [12,13,20,21]));
R = cellfun(@(x) ~isnumeric(x) && ~islogical(x),rawNumericColumns);
rawNumericColumns(R) = {NaN};
for catIdx = [1,2,3,4]
    idx = (rawStringColumns(:, catIdx) == "<undefined>");
    rawStringColumns(idx, catIdx) = "";
end
tabel = table;
tabel.VarName1 = cell2mat(rawNumericColumns(:, 1));
tabel.MMSI = cell2mat(rawNumericColumns(:, 2));
tabel.DATEUTC = dates{:, 1};
tabel.TIMEUTC = dates{:, 2};
tabel.SECONDI = hour(dates{:, 2})*3600+minute(dates{:, 2})*60+ second(dates{:, 2});
tabel.LAT = cell2mat(rawNumericColumns(:, 3));
tabel.LONG = cell2mat(rawNumericColumns(:, 4));
tabel.COURSE = cell2mat(rawNumericColumns(:, 5));
tabel.SPEED = cell2mat(rawNumericColumns(:, 6));
tabel.HEADING = cell2mat(rawNumericColumns(:, 7));
tabel.NAVSTAT = cell2mat(rawNumericColumns(:, 8));
tabel.IMO = cell2mat(rawNumericColumns(:, 9));
tabel.NAME = categorical(rawStringColumns(:, 1));
tabel.CALLSIGN = categorical(rawStringColumns(:, 2));
tabel.AISTYPE = cell2mat(rawNumericColumns(:, 10));
tabel.A = cell2mat(rawNumericColumns(:, 11));
tabel.B = cell2mat(rawNumericColumns(:, 12));
tabel.C = cell2mat(rawNumericColumns(:, 13));
tabel.D = cell2mat(rawNumericColumns(:, 14));
tabel.DRAUGHT = cell2mat(rawNumericColumns(:, 15));
tabel.DESTINATION = categorical(rawStringColumns(:, 3));
tabel.ETA = categorical(rawStringColumns(:, 4));
clearvars filename delimiter startRow formatSpec fileID dataArray ans raw col numericData rawData row regexstr result numbers invalidThousandsSeparator thousandsRegExp dateFormats dateFormatIndex dates blankDates anyBlankDates invalidDates anyInvalidDates rawNumericColumns rawStringColumns R catIdx idx;
f = 0;
while f == 0
flag = 0;
lung = length(tabel.DATEUTC);
while  flag == 0
A = input('Scegli anno da analizzare: ');
    for i=1:lung
    Y = year(tabel.DATEUTC);
    if Y(i) == A
        flag = 1;
    end
    end
    if flag == 0
        disp 'Anno non disponibile';
        disp 'Reinserisci anno';
    end
end
flag = 0;    
while  flag == 0
M = input('Scegli mese da analizzare: ');
    for i=1:lung
    M1 = month(tabel.DATEUTC);
    if M1(i) == M
        flag = 1;
    end
    end
    if flag == 0
        disp 'Mese non disponibile';
        disp 'Reinserisci mese';
    end
end
flag = 0;
while  flag == 0
D = input('scegli giorno da analizzare: ');
for i=1:lung
    D1 = day(tabel.DATEUTC);
    if D1(i) == D
        flag = 1;
    end
end
    if flag == 0
        disp 'giorno errato';
        disp 'reinserisci giorno';
    end
end
a = find(tabel.DATEUTC==(datetime(A,M,D)));
tab1 = tabel(a,:);
fi1 = tab1(end,:);
dati = tab1;
if (((max(tab1.SECONDI)) > 86099) && (fi1.SPEED ~= 0))
    b = find(tabel.DATEUTC==(datetime(2015,11,D+1))); 
    c = vertcat(a,b);
    tab2 = tabel(c,:);
    fi2 = tab1(end,:);
    dati = tab2;
    if (((max(tab2.SECONDI)) > 86099) && (fi2.SPEED ~= 0))
        e = find(tabel.DATEUTC==(datetime(2015,11,D+2))); 
        d = vertcat(c,e);
        dati = tabel(d,:);
    end    
end
flag = 0;
while flag == 0
 risp = input('Vuoi vedere il plot?[SI/NO] ','s');
 switch risp
     case 'si'
        figure(3);
        plot(dati.LONG,dati.LAT);
        xlabel 'LONG';
        ylabel 'LAT';
        flag = 1;
     case 'SI'
        figure(3);
        plot(dati.LONG,dati.LAT);
        xlabel 'LONG';
        ylabel 'LAT';
        flag = 1;
     case 'Si'
        figure(3);
        plot(dati.LONG,dati.LAT);
        xlabel 'LONG';
        ylabel 'LAT';
        flag = 1;
     case 'no'
         flag = 1;
     case 'NO'
         flag = 1;
     case 'No'
         flag = 1;
     otherwise 
         disp 'Error';
 end
 end
clearvars A D M a fi1 tab1 tab2 flag ;
%scegli classi
flag = 0;
while flag == 0
c = input('Quante classi vuoi visualizzare?[2/3/4] ');
if c > 4 || c < 2
    disp 'Errore'
else
    flag = 1;
end
end
tab = table;
tab.long = dati.LONG;
tab.lat = dati.LAT;
tab.vel = dati.SPEED;
[id,cen] = kmeans(tab.vel,c);
tab.classe = id;
tab.te = dati.SECONDI;
fin = length(tab.vel);
telim = 660;
for u = 2:(fin)
    deltat = (tab.te(u) - tab.te(u-1));
    if deltat > telim
        tab.trattov(u-1) = 1; % sconosciuto
    end
    if deltat <= telim
        if (tab.classe(u-1) ~= tab.classe(u))
            tab.trattov(u-1) = 2; %incerto
        end
        if (tab.classe(u) == tab.classe(u-1))
            if tab.classe(u-1) == 1
                tab.trattov(u-1) = ordin(1,cen);
            end
            if tab.classe(u-1) == 2                
                tab.trattov(u-1) = ordin(2,cen); 
            end                        
            if tab.classe(u-1) == 3
                tab.trattov(u-1) = ordin(3,cen);                               
            end
            if tab.classe(u-1) == 4
                tab.trattov(u-1) = ordin(4,cen);                               
            end
        end
    end
end
deltat = (tab.te(fin) - tab.te(fin-1));
    if deltat > telim
        tab.trattov(fin) = 1; % sconosciuto
    end
    if deltat <= telim
        if (tab.classe(fin) ~= tab.classe(fin-1))
            tab.trattov(fin) = 2; %incerto
        end
        if (tab.classe(fin) == tab.classe(fin-1))
            if tab.classe(fin) == 1
                tab.trattov(fin) = ordin(1,cen);
            end
            if tab.classe(fin) == 2                
                tab.trattov(fin) = ordin(2,cen); 
            end                        
            if tab.classe(fin) == 3
                tab.trattov(fin) = ordin(3,cen);                               
            end
            if tab.classe(fin) == 4
                tab.trattov(fin) = ordin(4,cen);                               
            end 
        end
    end
    
tab.course = dati.COURSE;
fin = length(tab.course);
lim = 40;
for i = 2:(fin)
    if tab.course(i-1) > 270 && tab.course(i) < 90 || tab.course(i-1) < 90 && tab.course(i) > 270
        if tab.course(i-1) < 90
            tab.course(i-1) = tab.course(i-1) + 360;
        end
        if tab.course(i) < 90
            tab.course(i) = tab.course(i) + 360;
        end
    end 
    x(i-1) = abs(tab.course(i) - tab.course(i-1));
    if tab.course(i-1) > 360
       
        tab.course(i-1) = tab.course(i-1) - 360;
    end
    if tab.course(i) > 360
        tab.course(i) = tab.course(i) - 360;        
    end
    if x(i-1) < lim
        y(i-1) = 1; %costante
    else
        y(i-1) = 0; %non costante
    end
end
if tab.course(fin-1) > 270 && tab.course(fin) < 90 || tab.course(fin-1) < 90 && tab.course(fin) > 270
        if tab.course(fin-1) < 90
            tab.course(fin-1) = tab.course(fin-1) + 360;
        end
        if tab.course(fin) < 90
            tab.course(fin) = tab.course(fin) + 360;
        end
end 
if tab.course(fin-1) > 360       
        tab.course(fin-1) = tab.course(fin-1) - 360;
    end
    if tab.course(fin) > 360
        tab.course(fin) = tab.course(fin) - 360;        
    end
x(fin) = abs(tab.course(fin) - tab.course(fin-1));
if x(fin) < lim
        y(fin) = 1; %costante
    else
        y(fin) = 0; %non costante
end
tab.trattoc = y';
flag = 0;
while flag == 0
risp = input( 'vuoi visualizzare gli istogrammi delle velocit�?[SI/NO]: ','s');
switch risp 
    case 'SI'
        istog(tab,c,id);
        flag = 1;
    case 'si'
        istog(tab,c,id);
        flag = 1;
    case 'Si'
        istog(tab,c,id);
        flag = 1;
    case 'No'
        flag = 1;
    case 'NO'
        flag = 1;
    case 'no'
        flag = 1;
    otherwise
        disp 'error';
end
end
flag = 0;
while flag == 0
risp = input( 'vuoi visualizzare i plot?[SI/NO]: ','s');
    switch risp 
        case 'SI' 
            gra(tab);
            flag = 1;
        case 'si'
            gra(tab)
            flag = 1;
        case 'Si'
            gra(tab)
            flag = 1;
        case 'no'
            flag = 1;
        case 'No'
            flag = 1;
        case 'NO'
            flag = 1;
        otherwise
            disp 'error';
    end
end 
clearvars c cen deltat fin i id lim tab telim u x y b d e fi2 risp tabella dati flag D1 lung M1 Y;
flag = 1;
while flag == 1
risp = input('Vuoi analizzare un altro giorno?[SI/NO] ','s');
switch risp
    case 'SI'
        f = 0;
        flag = 0;
    case 'si'
        f = 0;
        flag = 0;
    case 'Si'
        f = 0;
        flag = 0;
    case 'NO'
        f = 1;
        flag = 0;
        clearvars tabel;
    case 'no'
        f = 1;
        flag = 0;
        clearvars tabel;
    case 'No'
        f = 1;
        flag = 0;
        clearvars tabel;
    otherwise
        disp 'error';
end
end
clearvars risp flag
end
flag = 0;
while flag == 0
risp = input('Vuoi analizzare un altro file?[SI/NO]: ','s');
switch risp
    case 'si'
        fl = 0;
        flag = 1;
    case 'Si'
         fl = 0;
         flag = 1;
    case 'SI'
         fl = 0;
         flag = 1;
    case 'No'
         fl = 1;
         flag = 1;
    case 'NO'
         fl = 1;
         flag = 1;
    case 'no'
         fl = 1;
         flag = 1;
    otherwise
        disp 'Errore'
        flag = 0;
end
end

clearvars flag risp;
end
clearvars fl f;
function istog(dati,cl,ind)
figure(1);
legenda = [0.1 0.1 0.1; 0.5 0 0; 0.8 0.8 0; 0 0 1];
ran = [0.1 ; 0.25 ; 0.5 ; 0.75 ; 1 ; 1.15];
for s=1:6
    r = ran(s);
    subplot(2,3,s);
    hold on;
for k = 1:cl
    tab = dati.vel(find(ind == k),:);
    tprov = [min(tab):r:max(tab)];
    x = tprov' ;
    fy = length(tab);
    y = zeros(1,fy);
    for j = 1:fy
        temp = tab(j);
        if temp >= max(x)
            temp = max(x);
            y(j) = temp;        
        end
        if temp <= min(x)
            temp = min(x);
            y(j) = temp;
        end
        if y(j) == 0
            mi = (find(x>=temp));
            y(j) = (x(min(mi)));    
        end
    end
    hold on;
    h(k)=histogram(y,min(y):r:max(y));
    h(k).FaceColor = legenda(k,:);
    xlabel 'Velocit�';          
end
end
end
function gra(tab) %richiamata in tratto.m
figure(2);
finx = length(tab.vel);
subplot(2,1,1);
hold on;
for j = 1:(finx-1)
    tabp(1,:) = tab(j,:);
    tabp(2,:) = tab(j+1,:);
    x = tabp.trattov(1);
    switch x
        case 1
             plot(tabp.long,tabp.lat, '-k') %sconusciuto(nero) 
        case 2
             plot(tabp.long,tabp.lat, '-y') %incerto(giallo)
        case 3
             plot(tabp.long,tabp.lat, '-g') %bassissima vel(verde)
        case 4
             plot(tabp.long,tabp.lat, '-r') %bassa vel(rosso)
        case 5
             plot(tabp.long,tabp.lat, '-b') %media vel(blu)
        case 6
             plot(tabp.long,tabp.lat, '-m') %alta vel(magenta)
    end
end
tabp(1,:) = tab(finx-1,:);
tabp(2,:) = tab(finx,:);
x = tabp.trattov(2);
    switch x
        case 1
             plot(tabp.long,tabp.lat, '-k') %sconusciuto
        case 2
             plot(tabp.long,tabp.lat, '-y') %incerto
        case 3
             plot(tabp.long,tabp.lat, '-g') %bassissima vel
        case 4
             plot(tabp.long,tabp.lat, '-r') %bassa vel
        case 5
             plot(tabp.long,tabp.lat, '-b') %media vel
        case 6
             plot(tabp.long,tabp.lat, '-m') %alta vel
    end
subplot(2,1,2);
hold on;
for j = 1:(finx-1)
    tabp(1,:) = tab(j,:);
    tabp(2,:) = tab(j+1,:);
    x = tabp.trattoc(2);
    switch x
        case 0
             plot(tabp.long,tabp.lat, '-k') %sconusciuto(nero) 
        case 1
             plot(tabp.long,tabp.lat, '-r') %alta vel(rosso)
    end
end
tabp(1,:) = tab(finx-1,:);
tabp(2,:) = tab(finx,:);
x = tabp.trattoc(2);
    switch x
        case 0
             plot(tabp.long,tabp.lat, '-k') %sconusciuto
        case 1
             plot(tabp.long,tabp.lat, '-r') %incerto
    end
end
function r = ordin(id,cen)
 x = sort(cen);
 temp = cen(id);
 l = length(cen);
 for j = 1:l
     if x(j) == temp
         y = j;
     end
 end
 r = y + 2;
end