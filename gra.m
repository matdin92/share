function gra(tab) %richiamata in tratto.m
figure(2);
finx = length(tab.vel);
subplot(2,1,1);
hold on;
for j = 1:(finx-1)
    tabp(1,:) = tab(j,:);
    tabp(2,:) = tab(j+1,:);
    x = tabp.trattov(1);
    switch x
        case 1
             plot(tabp.long,tabp.lat, '-k') %sconusciuto(nero) 
        case 2
             plot(tabp.long,tabp.lat, '-y') %incerto(giallo)
        case 3
             plot(tabp.long,tabp.lat, '-g') %bassissima vel(verde)
        case 4
             plot(tabp.long,tabp.lat, '-r') %bassa vel(rosso)
        case 5
             plot(tabp.long,tabp.lat, '-b') %media vel(blu)
        case 6
             plot(tabp.long,tabp.lat, '-m') %alta vel(magenta)
    end
end
tabp(1,:) = tab(finx-1,:);
tabp(2,:) = tab(finx,:);
x = tabp.trattov(2);
    switch x
        case 1
             plot(tabp.long,tabp.lat, '-k') %sconusciuto
        case 2
             plot(tabp.long,tabp.lat, '-y') %incerto
        case 3
             plot(tabp.long,tabp.lat, '-g') %bassissima vel
        case 4
             plot(tabp.long,tabp.lat, '-r') %bassa vel
        case 5
             plot(tabp.long,tabp.lat, '-b') %media vel
        case 6
             plot(tabp.long,tabp.lat, '-m') %alta vel
    end
subplot(2,1,2);
hold on;
for j = 1:(finx-1)
    tabp(1,:) = tab(j,:);
    tabp(2,:) = tab(j+1,:);
    x = tabp.trattoc(2);
    switch x
        case 0
             plot(tabp.long,tabp.lat, '-k') %sconusciuto(nero) 
        case 1
             plot(tabp.long,tabp.lat, '-r') %alta vel(rosso)
    end
end
tabp(1,:) = tab(finx-1,:);
tabp(2,:) = tab(finx,:);
x = tabp.trattoc(2);
    switch x
        case 0
             plot(tabp.long,tabp.lat, '-k') %sconusciuto
        case 1
             plot(tabp.long,tabp.lat, '-r') %incerto
    end
end