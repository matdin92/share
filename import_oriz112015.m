%% Initialize variables.
filename = 'C:\Users\matteo\Desktop\Tirocinio\xADRI_trawlerORIZZONTE\xADRI_trawlerORIZZONTE\orizzonte_11_2015.csv';
delimiter = ',';
startRow = 2;

%% Read columns of data as text:
% For more information, see the TEXTSCAN documentation.
formatSpec = '%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%[^\n\r]';

%% Open the text file.
fileID = fopen(filename,'r');

%% Read columns of data according to the format.
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string', 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');

%% Close the text file.
fclose(fileID);

%% Convert the contents of columns containing numeric text to numbers.
% Replace non-numeric text with NaN.
raw = repmat({''},length(dataArray{1}),length(dataArray)-1);
for col=1:length(dataArray)-1
    raw(1:length(dataArray{col}),col) = mat2cell(dataArray{col}, ones(length(dataArray{col}), 1));
end
numericData = NaN(size(dataArray{1},1),size(dataArray,2));

for col=[1,2,5,6,7,8,9,10,11,14,15,16,17,18,19]
    % Converts text in the input cell array to numbers. Replaced non-numeric
    % text with NaN.
    rawData = dataArray{col};
    for row=1:size(rawData, 1)
        % Create a regular expression to detect and remove non-numeric prefixes and
        % suffixes.
        regexstr = '(?<prefix>.*?)(?<numbers>([-]*(\d+[\,]*)+[\.]{0,1}\d*[eEdD]{0,1}[-+]*\d*[i]{0,1})|([-]*(\d+[\,]*)*[\.]{1,1}\d+[eEdD]{0,1}[-+]*\d*[i]{0,1}))(?<suffix>.*)';
        try
            result = regexp(rawData(row), regexstr, 'names');
            numbers = result.numbers;
            
            % Detected commas in non-thousand locations.
            invalidThousandsSeparator = false;
            if numbers.contains(',')
                thousandsRegExp = '^\d+?(\,\d{3})*\.{0,1}\d*$';
                if isempty(regexp(numbers, thousandsRegExp, 'once'))
                    numbers = NaN;
                    invalidThousandsSeparator = true;
                end
            end
            % Convert numeric text to numbers.
            if ~invalidThousandsSeparator
                numbers = textscan(char(strrep(numbers, ',', '')), '%f');
                numericData(row, col) = numbers{1};
                raw{row, col} = numbers{1};
            end
        catch
            raw{row, col} = rawData{row};
        end
    end
end

dateFormats = {'yyyy-MM-dd', 'HH:mm:ss'};
dateFormatIndex = 1;
blankDates = cell(1,size(raw,2));
anyBlankDates = false(size(raw,1),1);
invalidDates = cell(1,size(raw,2));
anyInvalidDates = false(size(raw,1),1);
for col=[3,4]% Convert the contents of columns with dates to MATLAB datetimes using the specified date format.
    try
        dates{col} = datetime(dataArray{col}, 'Format', dateFormats{col==[3,4]}, 'InputFormat', dateFormats{col==[3,4]}); %#ok<SAGROW>
    catch
        try
            % Handle dates surrounded by quotes
            dataArray{col} = cellfun(@(x) x(2:end-1), dataArray{col}, 'UniformOutput', false);
            dates{col} = datetime(dataArray{col}, 'Format', dateFormats{col==[3,4]}, 'InputFormat', dateFormats{col==[3,4]}); %#ok<SAGROW>
        catch
            dates{col} = repmat(datetime([NaN NaN NaN]), size(dataArray{col})); %#ok<SAGROW>
        end
    end
    
    dateFormatIndex = dateFormatIndex + 1;
    blankDates{col} = dataArray{col} == '';
    anyBlankDates = blankDates{col} | anyBlankDates;
    invalidDates{col} = isnan(dates{col}.Hour) - blankDates{col};
    anyInvalidDates = invalidDates{col} | anyInvalidDates;
end
dates = dates(:,[3,4]);
blankDates = blankDates(:,[3,4]);
invalidDates = invalidDates(:,[3,4]);

%% Split data into numeric and string columns.
rawNumericColumns = raw(:, [1,2,5,6,7,8,9,10,11,14,15,16,17,18,19]);
rawStringColumns = string(raw(:, [12,13,20,21]));


%% Replace non-numeric cells with NaN
R = cellfun(@(x) ~isnumeric(x) && ~islogical(x),rawNumericColumns); % Find non-numeric cells
rawNumericColumns(R) = {NaN}; % Replace non-numeric cells



%% Make sure any text containing <undefined> is properly converted to an <undefined> categorical
for catIdx = [1,2,3,4]
    idx = (rawStringColumns(:, catIdx) == "<undefined>");
    rawStringColumns(idx, catIdx) = "";
end

%% Create output variable
tabel = table;
tabel.VarName1 = cell2mat(rawNumericColumns(:, 1));
tabel.MMSI = cell2mat(rawNumericColumns(:, 2));
tabel.DATEUTC = dates{:, 1};
tabel.TIMEUTC = dates{:, 2};
tabel.SECONDI = hour(dates{:, 2})*3600+minute(dates{:, 2})*60+ second(dates{:, 2});
tabel.LAT = cell2mat(rawNumericColumns(:, 3));
tabel.LONG = cell2mat(rawNumericColumns(:, 4));
tabel.COURSE = cell2mat(rawNumericColumns(:, 5));
tabel.SPEED = cell2mat(rawNumericColumns(:, 6));
tabel.HEADING = cell2mat(rawNumericColumns(:, 7));
tabel.NAVSTAT = cell2mat(rawNumericColumns(:, 8));
tabel.IMO = cell2mat(rawNumericColumns(:, 9));
tabel.NAME = categorical(rawStringColumns(:, 1));
tabel.CALLSIGN = categorical(rawStringColumns(:, 2));
tabel.AISTYPE = cell2mat(rawNumericColumns(:, 10));
tabel.A = cell2mat(rawNumericColumns(:, 11));
tabel.B = cell2mat(rawNumericColumns(:, 12));
tabel.C = cell2mat(rawNumericColumns(:, 13));
tabel.D = cell2mat(rawNumericColumns(:, 14));
tabel.DRAUGHT = cell2mat(rawNumericColumns(:, 15));
tabel.DESTINATION = categorical(rawStringColumns(:, 3));
tabel.ETA = categorical(rawStringColumns(:, 4));

% For code requiring serial dates (datenum) instead of datetime, uncomment
% the following line(s) below to return the imported dates as datenum(s).

% orizzonte112015.DATEUTC=datenum(orizzonte112015.DATEUTC);
% orizzonte112015.TIMEUTC=datenum(orizzonte112015.TIMEUTC);

%% Clear temporary variables
clearvars filename delimiter startRow formatSpec fileID dataArray ans raw col numericData rawData row regexstr result numbers invalidThousandsSeparator thousandsRegExp dateFormats dateFormatIndex dates blankDates anyBlankDates invalidDates anyInvalidDates rawNumericColumns rawStringColumns R catIdx idx;