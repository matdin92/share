function istog(dati,cl,ind)
legenda = [0.1 0.1 0.1; 0.5 0 0; 0.8 0.8 0; 0 0 1];
ran = [0.1 ; 0.25 ; 0.5 ; 0.75 ; 1 ; 1.15];
for s=1:6
    r = ran(s);
    subplot(2,3,s);
    hold on;
for k = 1:cl
    tab = dati.vel(find(ind == k),:);
    tprov = [min(tab):r:max(tab)];
    x = tprov' ;
    fy = length(tab);
    y = zeros(1,fy);
    for j = 1:fy
        temp = tab(j);
        if temp >= max(x)
            temp = max(x);
            y(j) = temp;        
        end
        if temp <= min(x)
            temp = min(x);
            y(j) = temp;
        end
        if y(j) == 0
            mi = (find(x>=temp));
            y(j) = (x(min(mi)));    
        end
    end
    hold on;
    h(k)=histogram(y,min(y):r:max(y));
    h(k).FaceColor = legenda(k,:);
    xlabel 'Velocit�';          
end
end
end