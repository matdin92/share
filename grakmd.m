function grakmd(D,M,Y,c)
import_oriz112015;
a = find(orizzonte112015.DATEUTC==(datetime(Y,M,D)));
tabella = orizzonte112015(a,:);
[i,x] = kmeans(tabella.SPEED,c);
figure;
hold on;
for j=1:c
plot(tabella.SPEED(i==j,1));
end
plot(x(:,1),'kx','MarkerSize',25)
ylabel 'Velocit�';
end