function tabella = plotday(dati,D) 
 a = find(dati.DATEUTC==(datetime(2015,11,D)));
 if a ~= 0
 tab1 = dati(a,:);
 fi1 = tab1(end,:);
 tabella = tab1;
 if (((max(tab1.SECONDI)) > 86099) && (fi1.SPEED ~= 0))
 b = find(dati.DATEUTC==(datetime(2015,11,D+1))); 
 c = vertcat(a,b);
 tab2 = dati(c,:);
 fi2 = tab1(end,:);
 tabella = tab2;
    if (((max(tab2.SECONDI)) > 86099) && (fi2.SPEED ~= 0))
    e = find(dati.DATEUTC==(datetime(2015,11,D+2))); 
    d = vertcat(c,e);
    tabella = dati(d,:);
    end    
 end
 plot(tabella.LONG,tabella.LAT);
 xlabel 'LONG';
 ylabel 'LAT';
 else
 disp 'giorno non trovato';
 tabella = 0;
 end
end