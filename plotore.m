function tabella2 = plotore(dati,YY,MM,DD,OI,MI,OF,MF) %(tabelladati,Anno,Mese,Giorno,Ora iniziale,Minuto iniziale,Ora Finale,Minuto Finale
b = dati.DATEUTC==(datetime(YY,MM,DD));
tabella1 = dati(b,:);
c = find(tabella1.SECONDI >= (OI*3600+MI*60) & tabella1.SECONDI <= (OF*3600+MF*60));
tabella2 = tabella1(c,:);
plot(tabella2.LONG,tabella2.LAT);
end