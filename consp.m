function r = consp(i,cen) %imposta il colore del tratto in base alla velocit�
if cen(1)>cen(2) && cen(2) > cen(3) && cen(1) > cen(3)
    switch i
        case 1 
            r = 5;
        case 2
            r = 3;
        case 3
            r = 4;
    end
end
if cen(1)>cen(2) && cen(2) < cen(3) && cen(1) > cen(3)
    switch i
        case 1 
            r = 5;
        case 2
            r = 4;
        case 3
            r = 3;
    end
end
if cen(1) > cen(2) && cen(2) < cen(3) && cen(1) < cen(3)
    switch i
        case 1 
            r = 3;
        case 2
            r = 4;
        case 3
            r = 5;
    end
end
if cen(1) < cen(2) && cen(2) > cen(3) && cen(1) > cen(3)
    switch i
        case 1 
            r = 3;
        case 2
            r = 5;
        case 3
            r = 4;
    end
end
if cen(1) < cen(2) && cen(2) > cen(3) && cen(1) < cen(3)
    switch i
        case 1 
            r = 4;
        case 2
            r = 5;
        case 3
            r = 3;
    end
end
if cen(1) < cen(2) && cen(2) < cen(3) && cen(1) < cen(3)
    switch i
        case 1 
            r = 4;
        case 2
            r = 3;
        case 3
            r = 5;
    end
end
end