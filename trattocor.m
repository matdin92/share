function tab = trattocor(dati)
tab = table;
tab.long = dati.LONG;
tab.lat = dati.LAT;
tab.vel = dati.SPEED;
tab.course = dati.COURSE;
fin = length(tab.course);
lim = 40;
for i = 2:(fin)
    if tab.course(i-1) > 270 && tab.course(i) < 90 || tab.course(i-1) < 90 && tab.course(i) > 270
        if tab.course(i-1) < 90
            tab.course(i-1) = tab.course(i-1) + 360;
        end
        if tab.course(i) < 90
            tab.course(i) = tab.course(i) + 360;
        end
    end 
    x(i-1) = abs(tab.course(i) - tab.course(i-1));
    if tab.course(i-1) > 360
       
        tab.course(i-1) = tab.course(i-1) - 360;
    end
    if tab.course(i) > 360
        tab.course(i) = tab.course(i) - 360;        
    end
    if x(i-1) < lim
        y(i-1) = 1; %costante
    else
        y(i-1) = 0; %non costante
    end
end
if tab.course(fin-1) > 270 && tab.course(fin) < 90 || tab.course(fin-1) < 90 && tab.course(fin) > 270
        if tab.course(fin-1) < 90
            tab.course(fin-1) = tab.course(fin-1) + 360;
        end
        if tab.course(fin) < 90
            tab.course(fin) = tab.course(fin) + 360;
        end
end 
if tab.course(fin-1) > 360       
        tab.course(fin-1) = tab.course(fin-1) - 360;
    end
    if tab.course(fin) > 360
        tab.course(fin) = tab.course(fin) - 360;        
    end
x(fin) = abs(tab.course(fin) - tab.course(fin-1));
if x(fin) < lim
        y(fin) = 1; %costante
    else
        y(fin) = 0; %non costante
end
tab.DIFF = x';
tab.tratto = y';
%gra(tab);
end